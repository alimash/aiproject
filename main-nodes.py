import socket
import struct
import random
from enum import Enum
from collections import defaultdict
'''
class Node: 
    def __init__(self, key): 
        self.left = None
        self.midleft = None
        self.right = None
        self.midright = None
        self.val = key
        self.direction = None
'''
class Node: 
    def __init__(self, uval = -100, dval = -100, rval = -100, lval = -100): 
        self.uval = uval
        self.dval = dval
        self.rval = rval
        self.lval = lval

class Directions(Enum):
    UP, DOWN, LEFT, RIGHT = 'UP', 'DOWN', 'LEFT', 'RIGHT'


class AI:

    def __init__(self, bot_id, bot_count, board_size, view_range, max_turns):
        self.__bot_id = bot_id
        self.__bot_count = bot_count
        self.__board_size = board_size
        self.__view_range = view_range
        self.__max_turns = max_turns

    def do_turn(self, board, bot_fruits, health):
        
        y_pos, x_pos = 0, 0
        for i in range(self.__board_size):
            for j in range(self.__board_size):
                if board[i][j] == str(self.__bot_id):
                    y_pos, x_pos = i, j

        scores = { 'A' : 4  , 'O' : 6 ,
                   'B' : 8  , 'W' : 8 ,
                   'C' : 10 , '.' : -1,
                   '*' : -100}

        U = scores[board[y_pos - 1][x_pos    ]]
        D = scores[board[y_pos + 1][x_pos    ]]
        R = scores[board[y_pos    ][x_pos + 1]]
        L = scores[board[y_pos    ][x_pos - 1]]
        if y_pos == 0:
            U = -101
        if x_pos == 0:
            L = -102
        if x_pos == self.__board_size - 1:
            R = -103
        if y_pos == self.__board_size - 1:
            D = -104

        
        #way finder
        #check if there is no fruit
        mylist = [U, D, R, L]
        print(mylist)
        if (max(mylist) <= -1):
            print("no fruit")
            '''
            mydict = defaultdict(list)
            for i,item in enumerate(mylist):
                mydict[item].append(i)
            mydict = {k:v for k,v in mydict.items() if len(v)>1}
            matchList = list(mydict.keys())
            print(matchList)
            '''
            index = 0
            indexList = []
            for items in mylist:
                if items == -1:
                    indexList.append(index)
                index += 1
            print(indexList)        
            randomChoice = random.choice(indexList)
            print(randomChoice)
            if randomChoice == 0:
                print("Up selected")
                return Directions.UP
            if randomChoice == 1:
                print("Down selected")
                return Directions.DOWN
            if randomChoice == 2:
                print("Right selected")
                return Directions.RIGHT
            if randomChoice == 3:
                print("Left selected")
                return Directions.LEFT
                
                
                

        
        
        
        
        root = Node(U, D, R, L)
        compareList = {'root.lval' : root.lval,
                       'root.uval' : root.uval,
                       'root.rval' : root.rval,
                       'root.dval' : root.dval}
        choice = max(compareList, key=compareList.get)
        if choice == 'root.uval':
            print('U')
            return Directions.UP
        if choice == 'root.dval':
            print('D')
            return Directions.DOWN
        if choice == 'root.lval':
            print('L')
            return Directions.LEFT
        if choice == 'root.rval':
            print('R')
            return Directions.RIGHT
        
        
        

        
            
        
        #return random.choice(list(Directions))


def read_utf(sock: socket.socket):
    length = struct.unpack('>H', sock.recv(2))[0]
    return sock.recv(length).decode('utf-8')


def write_utf(sock: socket.socket, msg: str):
    sock.send(struct.pack('>H', len(msg)))
    sock.send(msg.encode('utf-8'))


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 9898))
    init_data = read_utf(s)
    bot_id, bot_count, board_size, view_range, max_turns = map(int, init_data.split(','))
    ai = AI(bot_id, bot_count, board_size, view_range, max_turns)
    while True:
        health_str = read_utf(s)
        if health_str.startswith("WINNER"):
            print(health_str)
            break
        start_row, end_row, start_col, end_col = map(int, read_utf(s).split(','))
        board = []
        for i in range(board_size):
            row = []
            for j in range(board_size):
                row.append('.')
            board.append(row)
        board_str = read_utf(s)
        k = 0
        for i in range(start_row, end_row):
            for j in range(start_col, end_col):
                board[i][j] = board_str[k]
                k += 1
        fruits = [read_utf(s) for _ in range(bot_count)]
        write_utf(s, ai.do_turn(board, fruits, int(health_str)).value)


if __name__ == '__main__':
    main()
