import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

enum Direction {UP, DOWN, LEFT, RIGHT}

class AI {

    private final int botID, botCount, boardSize, viewRange, maxTurns;
    private Scanner input = new Scanner(System.in);

    AI(int botID, int botCount, int boardSize, int viewRange, int maxTurns) throws IOException {
        this.botID = botID;
        this.botCount = botCount;
        this.boardSize = boardSize;
        this.viewRange = viewRange;
        this.maxTurns = maxTurns;
    }

    Direction doTurn(char[][] board, String[] botFruits, int health) {
        return Direction.values()[(int) (Math.random() * Direction.values().length)];
    }

}

public class Main {

    public static void main(String[] args) {
        try {
            Socket s = new Socket("127.0.0.1", 9898);
            DataInputStream in = new DataInputStream(s.getInputStream());
            DataOutputStream out = new DataOutputStream(s.getOutputStream());
            String[] initData = in.readUTF().split(",");
            int botID = Integer.parseInt(initData[0]);
            int botCount = Integer.parseInt(initData[1]);
            int boardSize = Integer.parseInt(initData[2]);
            int viewRange = Integer.parseInt(initData[3]);
            int maxTurns = Integer.parseInt(initData[4]);
            AI ai = new AI(botID, botCount, boardSize, viewRange, maxTurns);
            String[] fruits = new String[botCount];
            while (true) {
                String healthStr = in.readUTF();
                if (healthStr.startsWith("WINNER")) {
                    System.out.println(healthStr);
                    break;
                }
                int health = Integer.parseInt(healthStr);
                System.out.println("HEALTH = " + health);
                String[] viewStr = in.readUTF().split(",");
                int startRow = Integer.parseInt(viewStr[0]);
                int endRow = Integer.parseInt(viewStr[1]);
                int startCol = Integer.parseInt(viewStr[2]);
                int endCol = Integer.parseInt(viewStr[3]);
                char[][] board = new char[boardSize][boardSize];
                for (int i = 0; i < boardSize; i++)
                    for (int j = 0; j < boardSize; j++)
                        board[i][j] = '.';
                String boardStr = in.readUTF();
                int k = 0;
                for (int i = startRow; i < endRow; i++) {
                    for (int j = startCol; j < endCol; j++) {
                        board[i][j] = boardStr.charAt(k++);
                        System.out.print(board[i][j]);
                    }
                    System.out.println();
                }
                for (int i = 0; i < botCount; ++i)
                    fruits[i] = in.readUTF();
                out.writeUTF(ai.doTurn(board, fruits, health).toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
