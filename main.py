import socket
import struct
import random
from enum import Enum


class Directions(Enum):
    UP, DOWN, LEFT, RIGHT = 'UP', 'DOWN', 'LEFT', 'RIGHT'


class AI:

    def __init__(self, bot_id, bot_count, board_size, view_range, max_turns):
        self.__bot_id = bot_id
        self.__bot_count = bot_count
        self.__board_size = board_size
        self.__view_range = view_range
        self.__max_turns = max_turns

    def do_turn(self, board, bot_fruits, health):
        
        #first find yourself
        current_pos = {"y" : 0, "x" : 0}
        y_counter, x_counter = int(0), int(0)
        for row in board:
            for item in row:
                if item == str(self.__bot_id):
                    print("if done")
                    current_pos["y"] = int(y_counter)
                    current_pos["x"] = int(x_counter)
                    print(current_pos)
                x_counter += 1
            y_counter += 1
            x_counter = 0
        #print(current_pos)
        
        #scan the surrounding
        surrounding = [['U', 'U'          , 'U'],
                       ['U', self.__bot_id, 'U'],
                       ['U', 'U'          , 'U']]#U = Unknown

        if current_pos["y"] == 0:
            if current_pos["x"] == 0:
                surrounding[1][2] = board[current_pos["y"]    ][current_pos["x"] + 1]
                surrounding[2][1] = board[current_pos["y"] + 1][current_pos["x"]    ]
                surrounding[2][2] = board[current_pos["y"] + 1][current_pos["x"] + 1]
            elif current_pos["x"] == self.__board_size - 1:
                surrounding[1][0] = board[current_pos["y"]    ][current_pos["x"] - 1]
                surrounding[2][0] = board[current_pos["y"] + 1][current_pos["x"] - 1]
                surrounding[2][1] = board[current_pos["y"] + 1][current_pos["x"]    ]
            else:
                surrounding[1][0] = board[current_pos["y"]    ][current_pos["x"] - 1]
                surrounding[1][2] = board[current_pos["y"]    ][current_pos["x"] + 1]
                surrounding[2][0] = board[current_pos["y"] + 1][current_pos["x"] - 1]
                surrounding[2][1] = board[current_pos["y"] + 1][current_pos["x"]    ]
                surrounding[2][2] = board[current_pos["y"] + 1][current_pos["x"] + 1]

        elif current_pos["y"] == self.__board_size - 1 :
            if current_pos["x"] == 0:
                surrounding[0][1] = board[current_pos["y"] - 1][current_pos["x"]    ]
                surrounding[0][2] = board[current_pos["y"] - 1][current_pos["x"] + 1]
                surrounding[1][2] = board[current_pos["y"]    ][current_pos["x"] + 1]
            elif current_pos["x"] == self.__board_size - 1:
                surrounding[0][1] = board[current_pos["y"] - 1][current_pos["x"]    ]
                surrounding[0][0] = board[current_pos["y"] - 1][current_pos["x"] - 1]
                surrounding[1][0] = board[current_pos["y"]    ][current_pos["x"] - 1]
            else:
                surrounding[0][0] = board[current_pos["y"] - 1][current_pos["x"] - 1]
                surrounding[0][1] = board[current_pos["y"] - 1][current_pos["x"]    ]
                surrounding[0][2] = board[current_pos["y"] - 1][current_pos["x"] + 1]
                surrounding[1][0] = board[current_pos["y"]    ][current_pos["x"] - 1]
                surrounding[1][2] = board[current_pos["y"]    ][current_pos["x"] + 1] 
            
        else:
            surrounding[0][0] = board[current_pos["y"] - 1][current_pos["x"] - 1]
            surrounding[0][1] = board[current_pos["y"] - 1][current_pos["x"]    ]
            surrounding[0][2] = board[current_pos["y"] - 1][current_pos["x"] + 1]
            surrounding[1][0] = board[current_pos["y"]    ][current_pos["x"] - 1]
            surrounding[1][2] = board[current_pos["y"]    ][current_pos["x"] + 1]
            surrounding[2][0] = board[current_pos["y"] + 1][current_pos["x"] - 1]
            surrounding[2][1] = board[current_pos["y"] + 1][current_pos["x"]    ]
            surrounding[2][2] = board[current_pos["y"] + 1][current_pos["x"] + 1]
            
        #print(surrounding)
'''
        surrounding = [['U', 'U'          , 'U'],
                       ['U', self.__bot_id, 'U'],
                       ['U', 'U'          , 'U']]#U = Unknown
'''
        #prioritizing
        scores = { 'A' : 4  , 'O' : 6 ,
                   'B' : 8  , 'W' : 8 ,
                   'C' : 10 , 'U' : 0 ,
                   0   : 0  }
        #decision
        if surrounding[0][1] == 'C':
            #################
            return Directions.UP
                    
        
        
        return random.choice(list(Directions))


def read_utf(sock: socket.socket):
    length = struct.unpack('>H', sock.recv(2))[0]
    return sock.recv(length).decode('utf-8')


def write_utf(sock: socket.socket, msg: str):
    sock.send(struct.pack('>H', len(msg)))
    sock.send(msg.encode('utf-8'))


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 9898))
    init_data = read_utf(s)
    bot_id, bot_count, board_size, view_range, max_turns = map(int, init_data.split(','))
    ai = AI(bot_id, bot_count, board_size, view_range, max_turns)
    while True:
        health_str = read_utf(s)
        if health_str.startswith("WINNER"):
            print(health_str)
            break
        start_row, end_row, start_col, end_col = map(int, read_utf(s).split(','))
        board = []
        for i in range(board_size):
            row = []
            for j in range(board_size):
                row.append('.')
            board.append(row)
        board_str = read_utf(s)
        k = 0
        for i in range(start_row, end_row):
            for j in range(start_col, end_col):
                board[i][j] = board_str[k]
                k += 1
        fruits = [read_utf(s) for _ in range(bot_count)]
        write_utf(s, ai.do_turn(board, fruits, int(health_str)).value)


if __name__ == '__main__':
    main()
