class Node: 
    def __init__(self, uval, dval, rval, lval): 
        self.uval = uval
        self.dval = dval
        self.rval = rval
        self.lval = lval

root = Node(10,
                    0,
                    7,
                    0)

compareList = {'root.lval' : root.lval,
               'root.uval' : root.uval,
               'root.rval' : root.rval,
               'root.dval' : root.dval}
max(compareList, key=compareList.get)


