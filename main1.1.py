import socket
import struct
import random
from enum import Enum


class Directions(Enum):
    UP, DOWN, LEFT, RIGHT = 'UP', 'DOWN', 'LEFT', 'RIGHT'


class AI:

    def __init__(self, bot_id, bot_count, board_size, view_range, max_turns):
        self.__bot_id = bot_id
        self.__bot_count = bot_count
        self.__board_size = board_size
        self.__view_range = view_range
        self.__max_turns = max_turns

    def do_turn(self, board, bot_fruits, health):
        y_pos, x_pos = 0, 0

        for i in range(self.__board_size):
            for j in range(self.__board_size):
                if board[i][j] == str(self.__bot_id):
                    y_pos, x_pos = i, j

        #prioritizing

        #1st score = 10
        if board[y_pos - 1][x_pos    ] == 'C':
            pass
        if board[y_pos + 1][x_pos    ] == 'C':
            pass
        if board[y_pos    ][x_pos - 1] == 'C':
            pass
        if board[y_pos    ][x_pos + 1] == 'C':
            pass

        #2nd score = 8
        if board[y_pos - 1][x_pos    ] == 'W' or board[y_pos - 1][x_pos    ] == 'B':
            pass
        if board[y_pos + 1][x_pos    ] == 'W' or board[y_pos + 1][x_pos    ] == 'B':
            pass
        if board[y_pos    ][x_pos - 1] == 'W' or board[y_pos    ][x_pos - 1] == 'B':
            pass
        if board[y_pos    ][x_pos + 1] == 'W' or board[y_pos    ][x_pos + 1] == 'B':
            pass

        #3nd score = 6
        if board[y_pos - 1][x_pos    ] == 'O':
            pass
        if board[y_pos + 1][x_pos    ] == 'O':
            pass
        if board[y_pos    ][x_pos - 1] == 'O':
            pass
        if board[y_pos    ][x_pos + 1] == 'O':
            pass

        #4th
        if board[y_pos - 1][x_pos    ] == 'A':
            pass
        if board[y_pos + 1][x_pos    ] == 'A':
            pass
        if board[y_pos    ][x_pos - 1] == 'A':
            pass
        if board[y_pos    ][x_pos + 1] == 'A':
            pass
        
        return random.choice(list(Directions))


def read_utf(sock: socket.socket):
    length = struct.unpack('>H', sock.recv(2))[0]
    return sock.recv(length).decode('utf-8')


def write_utf(sock: socket.socket, msg: str):
    sock.send(struct.pack('>H', len(msg)))
    sock.send(msg.encode('utf-8'))


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 9898))
    init_data = read_utf(s)
    bot_id, bot_count, board_size, view_range, max_turns = map(int, init_data.split(','))
    ai = AI(bot_id, bot_count, board_size, view_range, max_turns)
    while True:
        health_str = read_utf(s)
        if health_str.startswith("WINNER"):
            print(health_str)
            break
        start_row, end_row, start_col, end_col = map(int, read_utf(s).split(','))
        board = []
        for i in range(board_size):
            row = []
            for j in range(board_size):
                row.append('.')
            board.append(row)
        board_str = read_utf(s)
        k = 0
        for i in range(start_row, end_row):
            for j in range(start_col, end_col):
                board[i][j] = board_str[k]
                k += 1
        fruits = [read_utf(s) for _ in range(bot_count)]
        write_utf(s, ai.do_turn(board, fruits, int(health_str)).value)


if __name__ == '__main__':
    main()
